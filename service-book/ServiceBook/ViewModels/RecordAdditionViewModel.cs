﻿using Prism.Commands;
using System.Collections.Generic;
using System.Windows;
using static ServiceBook.ServiceRecord;

namespace ServiceBook.ViewModels
{
    /// <summary>
    /// Window for service record addition
    /// </summary>
    public class RecordAdditionViewModel
    {
        #region Fields

        private DatabaseLogic database;
        private MileageUnitToStringConverter mileageUnitConverter;
        private MileageUnit mileageUnit => (MileageUnit)mileageUnitConverter.ConvertBack(SelectedMileageUnit, typeof(MileageUnit), null, null);

        private Currency currencyUnit => (Currency)currencyTypeConverter.ConvertBack(SelectedCostCurrency, typeof(Currency), null, null);
        private CurrencyToStringConverter currencyTypeConverter;

        private RecordType recordType => (RecordType)recordTypeConverter.ConvertBack(SelectedRecordType, typeof(RecordType), null, null);
        private RecordTypeToStringConverter recordTypeConverter;

        private int mileage;
        private string selectedMileageUnit;
        private int price;
        private string selectedCostCurrency;
        private string name;
        private string selectedRecordType;
        private int vehicleID;

        #endregion


        #region Constructors

        public RecordAdditionViewModel(int Id, DatabaseLogic databaseLogic)
        {
            VehicleID = Id;
            database = databaseLogic;
            SelectedRecordType = "Fault";

            CostCurrencyList = ApplicationData.g_currencies;
            MileageUnitList = ApplicationData.g_mileageUnits;

            // zadat parametrem
            mileageUnitConverter = new MileageUnitToStringConverter();
            recordTypeConverter = new RecordTypeToStringConverter();
            currencyTypeConverter = new CurrencyToStringConverter();

            AddRecordCommand = new DelegateCommand<Window>(AddRecordHandler);
            CancelWindowCommand = new DelegateCommand<Window>(CancelWindowHandler);
        }

        #endregion


        #region Commands

        public DelegateCommand<Window> AddRecordCommand { get; set; }
        public DelegateCommand<Window> CancelWindowCommand { get; set; }

        #endregion


        #region Properties

        public List<string> CostCurrencyList { get; set; }

        /// <summary>
        /// List of length units which is shown
        /// </summary>
        public List<string> MileageUnitList { get; set; }

        public string SelectedRecordType
        {
            get => selectedRecordType;
            set
            {
                selectedRecordType = value;
            }
        }

        public string SelectedCostCurrency
        {
            get => selectedCostCurrency;
            set
            {
                selectedCostCurrency = value;
            }
        }

        public string SelectedMileageUnit
        {
            get => selectedMileageUnit;
            set
            {
                selectedMileageUnit = value;
            }
        }

        public string Name
        {
            get => name;
            set
            {
                name = value;
            }
        }

        public int Price
        {
            get => price;
            set
            {
                price = value;
            }
        }

        public int Mileage
        {
            get => mileage;
            set
            {
                mileage = value;
            }
        }

        public int VehicleID
        {
            get => vehicleID;
            set
            {
                vehicleID = value;
            }
        }

        #endregion


        #region Handlers

        public void AddRecordHandler(Window currentWindow)
        {
            // hardcoded hodnoty brát z radiobuttonů, přidat okno pro zadání měny, změnit okno pro mileage na textbox
            ServiceRecord toSave = new ServiceRecord(VehicleID, recordType, Name, Price, Mileage, currencyUnit, mileageUnit);
            database.SaveRecord(toSave);
            CloseWindow(currentWindow);
        }

        public void CancelWindowHandler(Window currentWindow)
        {
            CloseWindow(currentWindow);
        }

        #endregion


        #region Methods

        /// <summary>
        /// Closes current window
        /// </summary>
        /// <param name="currentWindow"></param>
        private void CloseWindow(Window currentWindow)
        {
            if (currentWindow != null)
            {
                currentWindow.Close();
            }
        }

        #endregion
    }
}
