﻿using Prism.Commands;
using System.Windows;
using static ServiceBook.Vehicle;
using System.Collections.Generic;
using ServiceBook.Helpers;
using ServiceBook.Database;

namespace ServiceBook.ViewModels
{
    /// <summary>
    /// Window for vehicle addition
    /// </summary>
    public class VehicleAdditionViewModel
    {
        #region Fields

        private DatabaseLogic database;
        private VehicleTypeToStringConverter vehicleTypeConverter;

        private string vehicleName;
        private string selectedVehicleType;
        private List<string> vehicleTypes;

        #endregion

        #region Constructors

        public VehicleAdditionViewModel(VehicleTypeToStringConverter converter)
        {
            // měl by být parametr, nikoliv hardcoded
            database = new DatabaseLogic(new ServiceBookDatabase());

            vehicleTypeConverter = converter;
            vehicleTypes = ApplicationData.g_vehicleTypes;

            AddVehicleCommand = new DelegateCommand<Window>(AddVehicleHandler);
            CancelWindowCommand = new DelegateCommand<Window>(CancelWindowHandler);
        }

        #endregion

        #region Commands

        public DelegateCommand<Window> AddVehicleCommand { get; set; }
        public DelegateCommand<Window> CancelWindowCommand { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// List of types of vehicles which are shown
        /// </summary>
        public List<string> VehicleTypes
        {
            get => vehicleTypes;
            private set
            {
                vehicleTypes = value;
            }
        }

        public string SelectedVehicleType
        {
            get => selectedVehicleType;
            set
            {
                selectedVehicleType = value;
            }
        }

        public string VehicleName
        {
            get => vehicleName;
            set
            {
                vehicleName = value;
            }
        }

        private VehicleType VehicleType
        {
            get => (VehicleType)vehicleTypeConverter.ConvertBack(SelectedVehicleType, typeof(VehicleType), null, null);
        }

        #endregion

        #region Handlers

        private void AddVehicleHandler(Window currentWindow)
        {
            if (VehicleName != null)
            {
                database.SaveVehicle(new Vehicle(VehicleName, VehicleType));
            }
            else
            {
                MessageViewer viewer = new MessageViewer();
                viewer.ShowErrorMessageBox(Properties.Resources.CantSaveEmptyParameters);
            }
            CloseWindow(currentWindow);
        }

        public void CancelWindowHandler(Window currentWindow)
        {
            CloseWindow(currentWindow);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Closes current window
        /// </summary>
        /// <param name="currentWindow"></param>
        private void CloseWindow(Window currentWindow)
        {
            if (currentWindow != null)
            {
                currentWindow.Close();
            }
        }

        #endregion
    }
}
