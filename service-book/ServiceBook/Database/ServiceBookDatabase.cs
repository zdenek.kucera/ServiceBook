﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBook.Database
{
    public class ServiceBookDatabase : DbContext
    {
        /// <summary>
        /// Vehicles table from database
        /// </summary>
        public DbSet<Vehicle> Vehicles { get; set; }

        /// <summary>
        /// ServiceRecords table from database
        /// </summary>
        public DbSet<ServiceRecord> Records { get; set; }

        public ServiceBookDatabase() : base()
        {

        }
    }
}
