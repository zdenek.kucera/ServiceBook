﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ServiceBook.Interfaces
{
    public interface IMessageShowable
    {
        void ShowErrorMessageBox(string message);
        MessageBoxResult ShowRecordDeletionMessageBox();
        MessageBoxResult ShowVehicleDeletionMessageBox();
    }
}
