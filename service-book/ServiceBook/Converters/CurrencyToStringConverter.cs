﻿using System;
using System.Globalization;
using System.Windows.Data;
using static ServiceBook.ServiceRecord;

public class CurrencyToStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (targetType == typeof(String))
        {
            switch (value)
            {
                case Currency.Dollar:
                    return "$";

                case Currency.Koruna:
                    return "Kč";

                case Currency.Euro:
                    return "€";

                case Currency.Pound:
                    return "Ł";
                
                default:
                    return null;
            }
        }
        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is string && targetType.IsEnum)
        {
            switch (value)
            {
                case "$":
                    return Currency.Dollar;

                case "€":
                    return Currency.Euro;

                case "Ł":
                    return Currency.Pound;

                case "Kč":
                    return Currency.Koruna;

                default:
                    return null;
            }
        }
        return null;
    }
}
