﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceBook.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ServiceBook.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        public static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not save. Some parameter is empty..
        /// </summary>
        public static string CantSaveEmptyParameters {
            get {
                return ResourceManager.GetString("CantSaveEmptyParameters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Car.
        /// </summary>
        public static string Car {
            get {
                return ResourceManager.GetString("Car", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fault.
        /// </summary>
        public static string Fault {
            get {
                return ResourceManager.GetString("Fault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Faults.
        /// </summary>
        public static string Faults {
            get {
                return ResourceManager.GetString("Faults", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Investment.
        /// </summary>
        public static string Investment {
            get {
                return ResourceManager.GetString("Investment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Investments.
        /// </summary>
        public static string Investments {
            get {
                return ResourceManager.GetString("Investments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mileage.
        /// </summary>
        public static string Mileage {
            get {
                return ResourceManager.GetString("Mileage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Motorcycle.
        /// </summary>
        public static string Motorcycle {
            get {
                return ResourceManager.GetString("Motorcycle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t connect to the database.
        ///Try to restart application. If problem remains, contact the support..
        /// </summary>
        public static string NullDatabaseLogicParameter {
            get {
                return ResourceManager.GetString("NullDatabaseLogicParameter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string Other {
            get {
                return ResourceManager.GetString("Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price.
        /// </summary>
        public static string Price {
            get {
                return ResourceManager.GetString("Price", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to quantity.
        /// </summary>
        public static string Quantity {
            get {
                return ResourceManager.GetString("Quantity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record addition.
        /// </summary>
        public static string RecordAddition {
            get {
                return ResourceManager.GetString("RecordAddition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete the selected service record?.
        /// </summary>
        public static string RecordDeletion {
            get {
                return ResourceManager.GetString("RecordDeletion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service record deletion.
        /// </summary>
        public static string RecordDeletionTitle {
            get {
                return ResourceManager.GetString("RecordDeletionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove.
        /// </summary>
        public static string Remove {
            get {
                return ResourceManager.GetString("Remove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to summary.
        /// </summary>
        public static string Summary {
            get {
                return ResourceManager.GetString("Summary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service book.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        public static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle.
        /// </summary>
        public static string Vehicle {
            get {
                return ResourceManager.GetString("Vehicle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle addition.
        /// </summary>
        public static string VehicleAddition {
            get {
                return ResourceManager.GetString("VehicleAddition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete the selected vehicle?.
        /// </summary>
        public static string VehicleDeletion {
            get {
                return ResourceManager.GetString("VehicleDeletion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle deletion.
        /// </summary>
        public static string VehicleDeletionTitle {
            get {
                return ResourceManager.GetString("VehicleDeletionTitle", resourceCulture);
            }
        }
    }
}
