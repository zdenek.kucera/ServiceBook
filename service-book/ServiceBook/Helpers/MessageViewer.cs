﻿using ServiceBook.Interfaces;
using System.Windows;

namespace ServiceBook.Helpers
{
    public class MessageViewer : IMessageShowable
    {
        public void ShowErrorMessageBox(string message)
        {
            MessageBox.Show(message, Properties.Resources.Error, MessageBoxButton.OKCancel, MessageBoxImage.Error);
        }

        public MessageBoxResult ShowRecordDeletionMessageBox()
        {
            return MessageBox.Show(Properties.Resources.RecordDeletion, Properties.Resources.RecordDeletionTitle, MessageBoxButton.YesNo, MessageBoxImage.Error);
        }

        public MessageBoxResult ShowVehicleDeletionMessageBox()
        {
            return MessageBox.Show(Properties.Resources.VehicleDeletion, Properties.Resources.VehicleDeletionTitle, MessageBoxButton.YesNo, MessageBoxImage.Error);
        }
    }
}
