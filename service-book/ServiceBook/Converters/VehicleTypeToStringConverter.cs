﻿using System;
using System.Globalization;
using System.Windows.Data;
using static ServiceBook.Vehicle;

public class VehicleTypeToStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (targetType == typeof(string))
        {
            switch (value)
            {
                case VehicleType.Car:
                    return "Car";

                case VehicleType.Motorcycle:
                    return "Motorcycle";

                case VehicleType.Other:
                    return "Other";

                default:
                    return null;
            }
        }
        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is string && targetType.IsEnum)
        {
            switch (value)
            {
                case "Car":
                    return VehicleType.Car;

                case "Motorcycle":
                    return VehicleType.Motorcycle;

                case "Other":
                    return VehicleType.Other;

                default:
                    return null;
            }
        }
        return null;
    }
}