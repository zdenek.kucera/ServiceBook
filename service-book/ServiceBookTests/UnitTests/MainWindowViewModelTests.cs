﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Prism.Commands;
using ServiceBook.Helpers;
using ServiceBook.Interfaces;
using ServiceBook.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static ServiceBook.ServiceRecord;
using static ServiceBook.Vehicle;

namespace ServiceBook.ViewModels.Tests
{
    [TestClass()]
    public class MainWindowViewModelTests
    {
        [TestMethod()]
        // tests if message box is shown after creation of MainWindowViewModel with null database logic parameter
        public void ConstructorTest()
        {
            var mockMessageViewer = new MockMessageViewer();
            var viewModel = new MainWindowViewModel(null, mockMessageViewer);
            Assert.IsTrue(mockMessageViewer.MessageShownCount == 1);
        }

        [TestMethod()]
        public void GetFilteredServiceRecordsCollectionTest()
        {
            var mockMessageViewer = new MockMessageViewer();
            var viewModel = new MainWindowViewModel(null, mockMessageViewer);
            var toFilter = new ObservableCollection<ServiceRecord>();
            var filteredCollection = new ObservableCollection<ServiceRecord>();

            toFilter.Add(new ServiceRecord(0, RecordType.Fault, "", 0, 0, Currency.Dollar, MileageUnit.Km));
            toFilter.Add(new ServiceRecord(0, RecordType.Fault, "", 0, 0, Currency.Dollar, MileageUnit.Km));
            toFilter.Add(new ServiceRecord(0, RecordType.Fault, "", 0, 0, Currency.Dollar, MileageUnit.Km));
            toFilter.Add(new ServiceRecord(0, RecordType.Investment, "", 0, 0, Currency.Dollar, MileageUnit.Km));


            filteredCollection = viewModel.GetFilteredServiceRecordsCollection(toFilter, RecordType.Fault);
            Assert.AreEqual(3, filteredCollection.Count);

            toFilter.Clear();

            toFilter.Add(new ServiceRecord(0, RecordType.Investment, "", 0, 0, Currency.Dollar, MileageUnit.Km));
            filteredCollection = viewModel.GetFilteredServiceRecordsCollection(toFilter, RecordType.Fault);
            Assert.AreEqual(0, filteredCollection.Count);

            toFilter.Clear();

            filteredCollection = viewModel.GetFilteredServiceRecordsCollection(toFilter, RecordType.Fault);
            Assert.AreEqual(0, filteredCollection.Count);
        }

        [TestMethod()]
        public void GetFilteredVehiclesCollectionTest()
        {
            var mockMessageViewer = new MockMessageViewer();
            var viewModel = new MainWindowViewModel(null, mockMessageViewer);
            var toFilter = new ObservableCollection<Vehicle>();
            var filteredCollection = new ObservableCollection<Vehicle>();

            toFilter.Add(new Vehicle("", VehicleType.Car));
            toFilter.Add(new Vehicle("", VehicleType.Car));
            toFilter.Add(new Vehicle("", VehicleType.Motorcycle));
            toFilter.Add(new Vehicle("", VehicleType.Other));

            filteredCollection = viewModel.GetFilteredVehiclesCollection(toFilter, VehicleType.Car);
            Assert.AreEqual(2, filteredCollection.Count);

            filteredCollection.Clear();

            toFilter.Add(new Vehicle("", VehicleType.Motorcycle));
            toFilter.Add(new Vehicle("", VehicleType.Other));
            Assert.AreEqual(0, filteredCollection.Count);

            filteredCollection.Clear();

            Assert.AreEqual(0, filteredCollection.Count);
        }

        [TestMethod()]
        public void CountTypesOfRecordsTest()
        {
            var mockMessageViewer = new MockMessageViewer();
            var viewModel = new MainWindowViewModel(null, mockMessageViewer);
            var collectionToCount = new ObservableCollection<ServiceRecord>();

            collectionToCount.Add(new ServiceRecord(0, RecordType.Fault, "", 10, 1, Currency.Dollar, MileageUnit.Km));
            collectionToCount.Add(new ServiceRecord(1, RecordType.Investment, "", 20, 2, Currency.Euro, MileageUnit.Mi));
            collectionToCount.Add(new ServiceRecord(0, RecordType.Fault, "", 10, 1, Currency.Koruna, MileageUnit.Mi));
            collectionToCount.Add(new ServiceRecord(1, RecordType.Investment, "", 20, 2, Currency.Pound, MileageUnit.Km));
            collectionToCount.Add(new ServiceRecord(1, RecordType.Investment, "", 20, 2, Currency.Pound, MileageUnit.Km));

            viewModel.CountTypesOfRecords(collectionToCount);
            Assert.AreEqual(2, viewModel.NumberOfFaults);
            Assert.AreEqual(3, viewModel.NumberOfInvestments);
            Assert.AreEqual(20, viewModel.FaultsSummaryPrice);
            Assert.AreEqual(60, viewModel.InvestmentsSummaryPrice);
            Assert.AreEqual(80, viewModel.AllRecordsSummaryPrice);

            collectionToCount.Clear();

            viewModel.CountTypesOfRecords(collectionToCount);
            Assert.AreEqual(0, viewModel.NumberOfFaults);
            Assert.AreEqual(0, viewModel.NumberOfInvestments);
            Assert.AreEqual(0, viewModel.FaultsSummaryPrice);
            Assert.AreEqual(0, viewModel.InvestmentsSummaryPrice);
            Assert.AreEqual(0, viewModel.AllRecordsSummaryPrice);
        }

        [TestMethod()]
        public void ChangeSelectedVehicleTypeTest()
        {
            var viewModel = new MainWindowViewModel(null, new MockMessageViewer());

            viewModel.ChangeSelectedVehicleType("Car");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Car);

            viewModel.ChangeSelectedVehicleType("Motorcycle");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Motorcycle);

            viewModel.ChangeSelectedVehicleType("Other");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Other);

            viewModel.ChangeSelectedVehicleType("aaaaaa");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Car);

            viewModel.ChangeSelectedVehicleType("");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Car);

            viewModel.ChangeSelectedVehicleType(" ");
            Assert.AreEqual(viewModel.SelectedVehicleType, VehicleType.Car);
        }

        [TestMethod()]
        public void ChangeSelectedRecordTypeTest()
        {
            var viewModel = new MainWindowViewModel(null, new MockMessageViewer());

            viewModel.ChangeSelectedRecordType("Fault");
            Assert.AreEqual(viewModel.SelectedRecordType, RecordType.Fault);

            viewModel.ChangeSelectedRecordType("Investment");
            Assert.AreEqual(viewModel.SelectedRecordType, RecordType.Investment);

            viewModel.ChangeSelectedRecordType("");
            Assert.AreEqual(viewModel.SelectedRecordType, RecordType.Fault);

            viewModel.ChangeSelectedRecordType(" ");
            Assert.AreEqual(viewModel.SelectedRecordType, RecordType.Fault);
        }
    }

    class MockMessageViewer : IMessageShowable
    {
        public MockMessageViewer()
        {
            MessageShownCount = 0;
        }

        public int MessageShownCount { get; set; }

        public void ShowErrorMessageBox(string message)
        {
            MessageShownCount++;
        }

        public MessageBoxResult ShowRecordDeletionMessageBox()
        {
            throw new NotImplementedException();
        }

        public MessageBoxResult ShowVehicleDeletionMessageBox()
        {
            throw new NotImplementedException();
        }
    }
}