﻿using System.Collections.Generic;

namespace ServiceBook
{
    /// <summary>
    /// Global data
    /// </summary>
    public static class ApplicationData
    {
        // in case of change - used in converter
        public static List<string> g_vehicleTypes = new List<string> { "Car", "Motorcycle", "Other"};
        public static List<string> g_mileageUnits = new List<string> { "Km", "Mi" };
        public static List<string> g_currencies = new List<string> { "$", "Kč", "€", "Ł"};
        public static List<string> g_recordTypes = new List<string> { "Fault", "Investment" };
    }
}
