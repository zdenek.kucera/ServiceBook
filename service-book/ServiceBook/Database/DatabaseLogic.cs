﻿using ServiceBook.Database;
using System.Collections.ObjectModel;
using System.Linq;

namespace ServiceBook
{
    /// <summary>
    /// Class prividing CRUD database operations
    /// </summary>
    public class DatabaseLogic
    {
        private ServiceBookDatabase database;

        public DatabaseLogic(ServiceBookDatabase databaseParam)
        {
            database = databaseParam;
        }

        public ObservableCollection<Vehicle> LoadVehicles()
        {
            var records = database.Vehicles;
            if (records != null)
            {
                return new ObservableCollection<Vehicle>(records);
            }
            else
            {
                return new ObservableCollection<Vehicle>();
            }
        }

        public ObservableCollection<ServiceRecord> LoadRecords(int vehicleID)
        {
            var records = database.Records.Where(x => x.VehicleID == vehicleID);
            ObservableCollection<ServiceRecord> toReturn = new ObservableCollection<ServiceRecord>(records);
            return toReturn;
        }

        public void SaveRecord(ServiceRecord toSave)
        {
            database.Records.Add(toSave);
            database.SaveChanges();
        }

        public void SaveVehicle(Vehicle toSave)
        {
            database.Vehicles.Add(toSave);
            database.SaveChanges();
        }

        public void DeleteVehicle(Vehicle toDelete)
        {
            // needed to attach before deletion because the entity was loaded by another instance of database
            database.Vehicles.Attach(toDelete);
            database.Vehicles.Remove(toDelete);
            database.SaveChanges();
        }

        public void DeleteRecord(ServiceRecord toDelete)
        {
            // needed to attach before deletion because the entity was loaded by another instance of database
            database.Records.Attach(toDelete);
            database.Records.Remove(toDelete);
            database.SaveChanges();
        }
    }
}
