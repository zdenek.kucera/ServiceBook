﻿using System;
using System.Globalization;
using System.Windows.Data;
using static ServiceBook.ServiceRecord;

public class RecordTypeToStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (targetType == typeof(string))
        {
            switch (value)
            {
                case RecordType.Fault:
                    return "Fault";

                case RecordType.Investment:
                    return "Investment";

                default:
                    return null;
            }
        }
        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is string && targetType.IsEnum)
        {
            switch (value)
            {
                case "Fault":
                    return RecordType.Fault;

                case "Investment":
                    return RecordType.Investment;

                default:
                    return null;
            }
        }
        return null;
    }
}