﻿using ServiceBook.ViewModels;
using System.Windows;

namespace ServiceBook
{
    /// <summary>
    /// Interaction logic for VehicleAddition.xaml
    /// </summary>
    public partial class VehicleAddition : Window
    {

        public VehicleAddition()
        {
            DataContext = new VehicleAdditionViewModel(new VehicleTypeToStringConverter());
            InitializeComponent();
        }
    }
}
