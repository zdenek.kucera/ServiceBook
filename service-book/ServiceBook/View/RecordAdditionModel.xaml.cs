﻿using ServiceBook.Database;
using ServiceBook.ViewModels;
using System.Windows;

namespace ServiceBook
{
    /// <summary>
    /// Interaction logic for RecordAddition.xaml
    /// </summary>
    public partial class RecordAddition : Window
    {
        public RecordAddition(int Id)
        {
            DataContext = new RecordAdditionViewModel(Id, new DatabaseLogic(new ServiceBookDatabase()));
            InitializeComponent();
        }
    }
}
