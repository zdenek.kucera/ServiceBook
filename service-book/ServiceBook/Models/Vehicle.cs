﻿namespace ServiceBook
{
    public class Vehicle
    {
        public enum VehicleType { Car, Motorcycle, Other };

        public int Id { get; set; }
        public string Name { get; set; }
        public VehicleType Type { get; set; }

        public Vehicle(string paramName, VehicleType paramType)
        {
            Name = paramName;
            Type = paramType;
        }

        public Vehicle(int paramId,string paramName, VehicleType paramType)
        {
            Id = paramId;
            Name = paramName;
            Type = paramType;
        }

        private Vehicle() { }
    }
}
