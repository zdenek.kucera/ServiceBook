﻿using System;
using System.Globalization;
using System.Windows.Data;
using static ServiceBook.ServiceRecord;

public class MileageUnitToStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (targetType == typeof(string))
        {
            switch (value)
            {
                case MileageUnit.Km:
                    return "Km";

                case MileageUnit.Mi:
                    return "Mi";

                default:
                    return null;
            }
        }
        return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is string && targetType.IsEnum)
        {
            switch (value)
            {
                case "Km":
                    return MileageUnit.Km;

                case "Mi":
                    return MileageUnit.Mi;

                default:
                    return null;
            }
        }
        return null;
    }
}