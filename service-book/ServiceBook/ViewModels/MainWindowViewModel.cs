﻿using Prism.Commands;
using ServiceBook.Helpers;
using ServiceBook.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using static ServiceBook.ServiceRecord;
using static ServiceBook.Vehicle;

namespace ServiceBook.ViewModels
{
    /// <summary>
    /// Main class
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Fields

        private DatabaseLogic database;
        private ObservableCollection<Vehicle> allVehiclesCollection;
        private ObservableCollection<ServiceRecord> allServiceRecordsCollection;
        private ObservableCollection<ServiceRecord> filteredServiceRecordsCollection;
        private ObservableCollection<Vehicle> filteredVehiclesCollection;
        private int allRecordsSummaryPrice;
        private int investmentsSummaryPrice;
        private int faultsSummaryPrice;
        private int numberOfInvestments;
        private int numberOfFaults;
        private int allRecordsCount;
        private RecordType selectedRecordType;
        private VehicleType selectedVehicleType;
        private ServiceRecord selectedServiceRecord;
        private Vehicle selectedVehicle;

        #endregion

        #region Constructors

        public MainWindowViewModel(DatabaseLogic paramDatabaseLogic, IMessageShowable messageViewer)
        {
            if (paramDatabaseLogic != null)
            {
                Initialize(paramDatabaseLogic);
            }
            else
            {
                messageViewer.ShowErrorMessageBox(Properties.Resources.NullDatabaseLogicParameter);
            }
        }

        #endregion

        #region Commands and events

        public DelegateCommand<object> AddVehicleCommand { get; set; }
        public DelegateCommand<object> DeleteVehicleCommand { get; set; }
        public DelegateCommand<object> EditVehicleCommand { get; set; }
        public DelegateCommand<string> VehicleTypeChangedCommand { get; set; }

        public DelegateCommand<object> AddRecordCommand { get; set; }
        public DelegateCommand<object> DeleteRecordCommand { get; set; }
        public DelegateCommand<object> EditRecordCommand { get; set; }
        public DelegateCommand<string> RecordTypeChangedCommand { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public RecordType SelectedRecordType
        {
            get => selectedRecordType;
            set
            {
                selectedRecordType = value;
                // selected record type changed, service records collection needs to be updated
                FilteredServiceRecordsCollection = GetFilteredServiceRecordsCollection(allServiceRecordsCollection, selectedRecordType);
                NotifyPropertyChanged();
            }
        }

        public VehicleType SelectedVehicleType
        {
            get => selectedVehicleType;
            set
            {
                selectedVehicleType = value;
                // selected vehicle type changed, vehicle collection needs to be updated
                FilteredVehiclesCollection = GetFilteredVehiclesCollection(allVehiclesCollection, SelectedVehicleType);
                NotifyPropertyChanged();
            }
        }

        public ServiceRecord SelectedServiceRecord
        {
            get => selectedServiceRecord;
            set
            {
                selectedServiceRecord = value;
                NotifyPropertyChanged();
            }
        }

        public Vehicle SelectedVehicle
        {
            get => selectedVehicle;
            set
            {
                // should be async
                if (value != null)
                {
                    selectedVehicle = value;
                    LoadServiceRecordsFromDatabase(value.Id);

                    // show only selected types of records
                    FilteredServiceRecordsCollection = GetFilteredServiceRecordsCollection(allServiceRecordsCollection, selectedRecordType);
                    AllRecordsCount = allServiceRecordsCollection.Count;
                    CountTypesOfRecords(allServiceRecordsCollection);
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Count of investments and faults together
        /// </summary>
        public int AllRecordsCount
        {
            get
            {
                return allRecordsCount;
            }
            set
            {
                allRecordsCount = value;
                NotifyPropertyChanged();
            }
        }

        public int NumberOfFaults
        {
            get => numberOfFaults;
            set
            {
                numberOfFaults = value;
                NotifyPropertyChanged();
            }
        }

        public int NumberOfInvestments
        {
            get => numberOfInvestments;
            set
            {
                numberOfInvestments = value;
                NotifyPropertyChanged();
            }
        }

        public int FaultsSummaryPrice
        {
            get => faultsSummaryPrice;
            set
            {
                faultsSummaryPrice = value;
                NotifyPropertyChanged();
            }
        }

        public int InvestmentsSummaryPrice
        {
            get => investmentsSummaryPrice;
            set
            {
                investmentsSummaryPrice = value;
                NotifyPropertyChanged();
            }
        }

        public int AllRecordsSummaryPrice
        {
            get => allRecordsSummaryPrice;
            set
            {
                allRecordsSummaryPrice = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Collection containing only vehicles of selected type
        /// </summary>
        public ObservableCollection<Vehicle> FilteredVehiclesCollection
        {
            get => filteredVehiclesCollection;
            set
            {
                filteredVehiclesCollection = value;

                if (filteredVehiclesCollection.Count > 0)
                {
                    SelectedVehicle = filteredVehiclesCollection[0];
                }

                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Collection containing only service records of selected type
        /// </summary>
        public ObservableCollection<ServiceRecord> FilteredServiceRecordsCollection
        {
            get => filteredServiceRecordsCollection;
            set
            {
                filteredServiceRecordsCollection = value;

                if (filteredServiceRecordsCollection.Count > 0)
                {
                    SelectedServiceRecord = filteredServiceRecordsCollection[0];
                }

                NotifyPropertyChanged();
            }
        }

        #endregion

        #region Handlers

        private void AddVehicleHandler()
        {
            var additionWindow = new VehicleAddition();
            additionWindow.Show();

            // after window closing, refresh the vehicles list
            additionWindow.Closed += new EventHandler(VehicleWindowClosingHandler);
        }

        private void AddRecordHandler()
        {
            var additionWindow = new RecordAddition(SelectedVehicle.Id);
            additionWindow.Show();
            // after window closing, refresh the service records list
            additionWindow.Closed += new EventHandler(RecordWindowClosingHandler);
        }

        private void EditVehicleHandler()
        {
            System.Windows.MessageBox.Show("Not implemented yet");
        }

        private void EditRecordHandler()
        {
            System.Windows.MessageBox.Show("Not implemented yet");
        }

        private void DeleteRecordHandler()
        {
            MessageViewer messageViewer = new MessageViewer();
            // ask user if he is sure to delete service record
            var result = messageViewer.ShowRecordDeletionMessageBox();

            if (result == MessageBoxResult.Yes)
            {
                database.DeleteRecord(SelectedServiceRecord);
            }

            LoadServiceRecordsFromDatabase(SelectedVehicle.Id);
            FilteredServiceRecordsCollection = FilterServiceRecordsCollection(allServiceRecordsCollection, SelectedRecordType);
        }

        private void DeleteVehicleHandler()
        {
            MessageViewer messageViewer = new MessageViewer();
            // ask user if he is sure to delete vehicle
            var result = messageViewer.ShowVehicleDeletionMessageBox();

            if (result == MessageBoxResult.Yes)
            {
                database.DeleteVehicle(SelectedVehicle);
            }

            LoadVehiclesFromDatabase();
            FilteredVehiclesCollection = FilterVehiclesCollection(allVehiclesCollection, SelectedVehicleType);
        }

        /// <summary>
        /// Refresh selected vehicle type, filters and refresh vehicles collection
        /// </summary>
        /// <param name="vehicleType">Selected vehicle type</param>
        // is private even if ChangeSelectedVehicleType is public because ChangeSelectedVehicleType should be accesed directly or through command not by this
        private void VehicleTypeChangedHandler(string vehicleType)
        {
            ChangeSelectedVehicleType(vehicleType);
        }

        /// <summary>
        /// Refresh selected service record type, filters and refresh records collection
        /// </summary>
        /// <param name="recordType">Selected service record type</param>
        // is private even if ChangeSelectedRecordType is public because ChangeSelectedRecordType should be accesed directly or through command not by this
        private void RecordTypeChangedHandler(string recordType)
        {
            ChangeSelectedRecordType(recordType);
        }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize(DatabaseLogic paramDatabaseLogic)
        {
            // should be in special method called Initialize properties()
            SelectedVehicleType = VehicleType.Car;
            SelectedRecordType = RecordType.Fault;

            database = paramDatabaseLogic;
            LoadVehiclesFromDatabase();

            // show only selected types of vehicles and records
            FilteredVehiclesCollection = GetFilteredVehiclesCollection(allVehiclesCollection, SelectedVehicleType);
            FilteredServiceRecordsCollection = GetFilteredServiceRecordsCollection(allServiceRecordsCollection, selectedRecordType);

            AddVehicleCommand = new DelegateCommand<object>(o => AddVehicleHandler());
            DeleteVehicleCommand = new DelegateCommand<object>(o => DeleteVehicleHandler());
            EditVehicleCommand = new DelegateCommand<object>(o => EditVehicleHandler());

            AddRecordCommand = new DelegateCommand<object>(o => AddRecordHandler());
            DeleteRecordCommand = new DelegateCommand<object>(o => DeleteRecordHandler());
            EditRecordCommand = new DelegateCommand<object>(o => EditRecordHandler());

            VehicleTypeChangedCommand = new DelegateCommand<string>(VehicleTypeChangedHandler);
            RecordTypeChangedCommand = new DelegateCommand<string>(RecordTypeChangedHandler);

            NotifyPropertyChanged();
        }

        public void ChangeSelectedVehicleType(string newVehicleType)
        {
            switch (newVehicleType)
            {
                case "Car":
                    SelectedVehicleType = VehicleType.Car;
                    break;

                case "Motorcycle":
                    SelectedVehicleType = VehicleType.Motorcycle;
                    break;

                case "Other":
                    SelectedVehicleType = VehicleType.Other;
                    break;

                default:
                    SelectedVehicleType = VehicleType.Car;
                    break;
            }
        }

        public void ChangeSelectedRecordType(string newRecordType)
        {
            switch (newRecordType)
            {
                case "Fault":
                    SelectedRecordType = RecordType.Fault;
                    break;

                case "Investment":
                    SelectedRecordType = RecordType.Investment;
                    break;

                default:
                    SelectedRecordType = RecordType.Fault;
                    break;
            }
        }

        /// <summary>
        /// Refreshes vehicles list after addition
        /// </summary>
        private void VehicleWindowClosingHandler(object sender, System.EventArgs e)
        {
            LoadVehiclesFromDatabase();
            FilteredVehiclesCollection = GetFilteredVehiclesCollection(allVehiclesCollection, SelectedVehicleType);
        }

        private void RecordWindowClosingHandler(object sender, System.EventArgs e)
        {
            LoadServiceRecordsFromDatabase(SelectedVehicle.Id);
            FilteredServiceRecordsCollection = FilterServiceRecordsCollection(allServiceRecordsCollection, SelectedRecordType);
        }


        /// <summary>
        /// Fills shown collection only with vehicles of selected type
        /// </summary>
        public ObservableCollection<Vehicle> GetFilteredVehiclesCollection(ObservableCollection<Vehicle> collectionToFilter, VehicleType selectedType)
        {
            var filteredCollection = new ObservableCollection<Vehicle>();

            filteredCollection = FilterVehiclesCollection(collectionToFilter, selectedType);

            return filteredCollection;
        }

        private ObservableCollection<Vehicle> FilterVehiclesCollection(ObservableCollection<Vehicle> collectionToFilter, VehicleType selectedType)
        {
            var filteredCollection = new ObservableCollection<Vehicle>();

            if (collectionToFilter != null)
            {
                foreach (Vehicle vehicle in collectionToFilter)
                {
                    if (vehicle.Type == selectedType)
                    {
                        filteredCollection.Add(vehicle);
                    }
                }
            }

            return filteredCollection;
        }

        /// <summary>
        /// Fills shown collection only with records of selected type
        /// </summary>
        public ObservableCollection<ServiceRecord> GetFilteredServiceRecordsCollection(ObservableCollection<ServiceRecord> collectionToFilter, RecordType selectedType)
        {
            var filteredCollection = new ObservableCollection<ServiceRecord>();
            filteredCollection = FilterServiceRecordsCollection(collectionToFilter, selectedType);

            return filteredCollection;
        }

        private ObservableCollection<ServiceRecord> FilterServiceRecordsCollection(ObservableCollection<ServiceRecord> collectionToFilter, RecordType selectedType)
        {
            var filteredCollection = new ObservableCollection<ServiceRecord>();

            if (collectionToFilter != null)
            {
                foreach (ServiceRecord record in collectionToFilter)
                {
                    if (record.Type == selectedType)
                    {
                        filteredCollection.Add(record);
                    }
                }
            }

            return filteredCollection;
        }

        private void LoadVehiclesFromDatabase()
        {
            allVehiclesCollection = database.LoadVehicles();
        }

        private void LoadServiceRecordsFromDatabase(int Id)
        {
            try
            {
                allServiceRecordsCollection = database.LoadRecords(Id);
            }
            catch (System.ArgumentOutOfRangeException)
            { }
        }

        /// <summary>
        /// Counts numbers of records of each record type and summary prices
        /// </summary>
        public void CountTypesOfRecords(ObservableCollection<ServiceRecord> collectionParameter)
        {
            NumberOfFaults = 0;
            NumberOfInvestments = 0;
            FaultsSummaryPrice = 0;
            InvestmentsSummaryPrice = 0;
            AllRecordsSummaryPrice = 0;

            foreach (ServiceRecord record in collectionParameter)
            {
                if (record.Type == RecordType.Fault)
                {
                    NumberOfFaults++;
                    FaultsSummaryPrice = FaultsSummaryPrice + record.Cost;
                }
                else
                {
                    NumberOfInvestments++;
                    InvestmentsSummaryPrice = InvestmentsSummaryPrice + record.Cost;
                }
                AllRecordsSummaryPrice = AllRecordsSummaryPrice + record.Cost;
            }
        }

        #endregion
    }
}
