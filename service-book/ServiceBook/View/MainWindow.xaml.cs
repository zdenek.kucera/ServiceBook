﻿using ServiceBook.ViewModels;
using ServiceBook.Database;
using System.Windows;
using ServiceBook.Helpers;

namespace ServiceBook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var viewModel = new MainWindowViewModel(new DatabaseLogic(new ServiceBookDatabase()), new MessageViewer());
            DataContext = viewModel;
        }
    }
}
