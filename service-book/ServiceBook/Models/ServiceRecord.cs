﻿namespace ServiceBook
{
    public class ServiceRecord
    {
        public enum RecordType { Investment, Fault };
        public enum MileageUnit { Km, Mi }
        public enum Currency { Dollar, Koruna, Pound, Euro};

        public int Id { get; set; }
        public int VehicleID { get; set; }
        public RecordType Type { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Mileage { get; set; }
        public Currency CostCurrency { get; set; }
        public MileageUnit TachometerUnit { get; set; }

        public ServiceRecord(int _vehicleID, RecordType _type, string _name, int _cost, int _mileage, Currency _currency, MileageUnit _mileageUnit)
        {
            VehicleID = _vehicleID;
            Type = _type;
            Name = _name;
            Cost = _cost;
            Mileage = _mileage;
            CostCurrency = _currency;
            TachometerUnit = _mileageUnit;
        }

        private ServiceRecord() { }
    }
}
